<?php

namespace App\Providers;

use App\Mixins\StrMixins;
use Illuminate\Support\Str;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Str::macro(
            'partNumber',
            function($part) {
                return 'AB-' . substr($part, 0, 3) . '-' . substr($part, 3);
            }
        );

        // Str::macro(
        //     'prefix',
        //     function($string, $prefix = 'AB-') {
        //         return $prefix . $string;
        //     }
        // );
        
        Str::mixin(
            new StrMixins(),
            false
        );

        ResponseFactory::macro(
            'errorJson',
            function($message = 'Default error message') {
                return [
                    'message' => $message,
                    'error_code' => 123
                ];
            }
        );
    }
}
