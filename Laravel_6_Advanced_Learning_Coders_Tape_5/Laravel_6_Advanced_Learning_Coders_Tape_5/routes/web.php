<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\ResponseFactory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/macro/partNumber', function () {
    dd(Str::partNumber('1234567890'));
});

Route::get('/macro/prefix', function () {
    dd(Str::prefix('1234567890'));
});

Route::get('/mixin/partNumber', function () {
    dd(Str::partNumber('1234567890'));
});

Route::get('/mixin/prefix', function () {
    dd(Str::prefix('1234567890'));
});

Route::get('/erorrJson', function () {
    return ResponseFactory::errorJson();
});