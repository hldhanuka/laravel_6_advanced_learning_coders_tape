<?php

namespace App\Providers;

use App\PostcardSendignService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(
            'Postcard',
            function($app) {
                return new PostcardSendignService('US', 4, 6);
            }
        );
    }
}
