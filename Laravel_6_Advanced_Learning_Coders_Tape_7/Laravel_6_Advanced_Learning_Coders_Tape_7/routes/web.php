<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CustomersController@index');

Route::get('/customer/{customerId}', 'CustomersController@show');

Route::get('/customer/{customerId}/update', 'CustomersController@update');

Route::get('/customer/{customerId}/delete', 'CustomersController@destroy');
