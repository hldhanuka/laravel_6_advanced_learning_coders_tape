<?php

namespace App\Providers;

use App\Channel;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Http\View\Composers\ChannelComposer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Option 1 - Every Single View
        // View::share('channels', Channel::orderBy('name')->get());

        // Option 2 - Granular view
        // View::composer(
        //     'channel.index',
        //     function($view) {
        //         $view->with(
        //             'channels',
        //             Channel::orderBy('name')->get()
        //         );
        //     }
        // );

        // // Option 2 - Granular multiple views
        // View::composer(
        //     ['channel.index', 'post.create'],
        //     function($view) {
        //         $view->with(
        //             'channels',
        //             Channel::orderBy('name')->get()
        //         );
        //     }
        // );

        // Option 2 - Granular multiple views with wildcards
        // View::composer(
        //     ['channel.*', 'post.*'],
        //     function($view) {
        //         $view->with(
        //             'channels',
        //             Channel::orderBy('name')->get()
        //         );
        //     }
        // );

        // // Option 3 - Dedicated Class
        // View::composer(
        //     ['channel.*', 'post.*'],
        //     ChannelComposer::class
        // );

        // Option 3 - Dedicated Class with views refactor
        View::composer(
            'partials.channels.*',
            ChannelComposer::class
        );
    }
}
